using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetBlinkingPos : MonoBehaviour
{
  public GameObject parentObject;
  public GameObject lefteyepos;
  public GameObject lefteyebottompos;
  public float rightTopEyeLidPoint, rightBottomEyeLidPoint, leftTopEyeLidPoint, leftBottomEyeLidPoint, referencePoint1, referencePoint2, ratio1, ratio2, midpointRatio;
  public bool eyesOpen = true, rightEyeClosed = false, leftEyeClosed = false, bothEyesClosed = false;

  public Text eyesDitactor;

  // Start is called before the first frame update
  void Start()
  {
    referencePoint1 = parentObject.transform.GetChild(5).transform.position.y;
    referencePoint2 = parentObject.transform.GetChild(4).transform.position.y;

    rightTopEyeLidPoint = parentObject.transform.GetChild(386).transform.position.y;
    rightBottomEyeLidPoint = parentObject.transform.GetChild(373).transform.position.y;

    leftTopEyeLidPoint = parentObject.transform.GetChild(159).transform.position.y;
    leftBottomEyeLidPoint = parentObject.transform.GetChild(145).transform.position.y;

    //lefteyepos = parentObject.transform.GetChild(159).transform.gameObject;
    //lefteyebottompos = parentObject.transform.GetChild(145).transform.gameObject;

    //Debug.Log(parentObject.transform.GetChildCount());
    //Debug.Log(parentObject.transform.GetChild(386).localPosition.y);

    //childObjects.Add(parentObject.GetComponentsInChildren<GameObject>());
   // Debug.Log(childObjects[386]);
  }

  // Update is called once per frame
  void Update()
  {

    Calculation();
    EyesBlinkDetector();
    //RightEyeBlinkDetector();
    //LeftEyeBlinkDetector();

    //leftTopEyeLidPoint = parentObject.transform.GetChild(159).transform.position.y;
    //leftBottomEyeLidPoint = parentObject.transform.GetChild(145).transform.position.y;

    //ratio2 = (leftTopEyeLidPoint - leftBottomEyeLidPoint) / (referencePoint2 - referencePoint1);
    //Debug.Log(ratio2);
    //Debug.Log(ratio);
  }

  public void Calculation()
  {
    referencePoint1 = parentObject.transform.GetChild(5).transform.position.y;
    referencePoint2 = parentObject.transform.GetChild(4).transform.position.y;

    rightTopEyeLidPoint = parentObject.transform.GetChild(386).transform.position.y;
    rightBottomEyeLidPoint = parentObject.transform.GetChild(373).transform.position.y;

    leftTopEyeLidPoint = parentObject.transform.GetChild(159).transform.position.y;
    leftBottomEyeLidPoint = parentObject.transform.GetChild(145).transform.position.y;

    ratio1 = (rightBottomEyeLidPoint - rightTopEyeLidPoint) / (referencePoint2 - referencePoint1);
    ratio2 = (leftBottomEyeLidPoint - leftTopEyeLidPoint) / (referencePoint2 - referencePoint1);

    midpointRatio = (ratio1 + ratio2) / 2f;
    //Debug.Log(midpointRatio);
  }

  public void EyesBlinkDetector()
  {
        
    if (midpointRatio < 0.5f)
    {
      rightEyeClosed = true;
      leftEyeClosed = true;
      eyesOpen = false;

      if (rightEyeClosed && leftEyeClosed)
      {
        Debug.Log("Eyes Closed");
        eyesDitactor.text = "Eyes Closed";
        rightEyeClosed = false;
        leftEyeClosed = false;
        eyesOpen = true;
      }
    }
    else
    {
      if (eyesOpen)
      {
        Debug.Log("Eyes Open");
        eyesDitactor.text = "Eyes Open";
        rightEyeClosed = true;
        leftEyeClosed = true;
        eyesOpen = false;
      }
    }
  }

  public void RightEyeBlinkDetector()
  {

    if (ratio1 < 0.75 && ratio2 > 0.5)
    {
      rightEyeClosed = true;
      leftEyeClosed = false;
      eyesOpen = false;

      if (rightEyeClosed && !leftEyeClosed)
      {
        Debug.Log("Right Eye Closed");
        eyesDitactor.text = "Right Eye Closed";
        rightEyeClosed = false;
        leftEyeClosed = false;
        eyesOpen = true;
      }
    }
    else
    {
      if (eyesOpen)
      {
        Debug.Log("Eyes Open");
        eyesDitactor.text = "Eyes Open";
        rightEyeClosed = true;
        leftEyeClosed = true;
        eyesOpen = false;
      }
    }
  }

  public void LeftEyeBlinkDetector()
  {
    //Debug.Log(ratio2);

    if (ratio2 < 0.75 && ratio1 > 0.5)
    {
      rightEyeClosed = false;
      leftEyeClosed = true;
      eyesOpen = false;

      if (leftEyeClosed && !rightEyeClosed)
      {
        Debug.Log("Left Eye Closed");
        eyesDitactor.text = "Left Eye Closed";
        leftEyeClosed = false;
        rightEyeClosed = false;
        eyesOpen = true;
      }
    }

    else
    {
      if (eyesOpen)
      {
        Debug.Log("Eyes Open");
        eyesDitactor.text = "Eyes Open";
        rightEyeClosed = true;
        leftEyeClosed = true;
        eyesOpen = false;
      }
    }
  }



}
