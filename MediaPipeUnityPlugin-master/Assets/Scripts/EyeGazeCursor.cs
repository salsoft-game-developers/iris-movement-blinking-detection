using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeGazeCursor : MonoBehaviour
{
  public GameObject leftIris, rightIris;

  private Camera camera;
  RaycastHit hitL, hitR;
  public Transform leftEye, rightEye, leftEyeEndPoint;
  Vector3 averageHitPoint;
  /*Vector3*/ public Transform midPoint;
  private LineRenderer laserLine;
  private LineRenderer laserLine2;
  private LineRenderer laserLine3;

  bool instantiateOnce;
  
  // Start is called before the first frame update
  void Start()
  {
    instantiateOnce = true;
    camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    laserLine = this.transform.GetChild(0).GetComponent<LineRenderer>();
    laserLine2 = this.transform.GetChild(1).GetComponent<LineRenderer>();
    laserLine3 = this.transform.GetChild(2).GetComponent<LineRenderer>();
  }

  // Update is called once per frame
  void Update()
    {
    if (leftEye != null && rightEye != null)
    {
      CastEyeRays();
    }
    else
    { SearchEyes(); }
  }

  void SearchEyes()
  {
    if (leftIris.transform.GetChild(0) != null)
    {
      leftEye = leftIris.transform.GetChild(0).transform;
      if(instantiateOnce == true)
      {
        leftEyeEndPoint = Instantiate(midPoint, new Vector3(leftEye.position.x, leftEye.position.y, leftEye.position.z - 100f), Quaternion.identity);
        instantiateOnce = false;
      }
    }
    if (rightIris.transform.GetChild(0) != null)
    {
      rightEye = rightIris.transform.GetChild(0).transform;
    }

  }

  public void CastEyeRays()
  {
    laserLine.SetPosition(0, leftEye.position);
    //laserLine.SetPosition(1, leftEye.position+ -(leftEye.transform.forward * 100));

    //laserLine2.SetPosition(0, rightEye.position);
    //laserLine2.SetPosition(1, rightEye.position + -(leftEye.transform.forward * 100));

    midPoint.position = (leftEye.position + rightEye.position) / 2f;
    //laserLine3.SetPosition(0, midPoint.position);
    //laserLine3.SetPosition(1, midPoint.position + -(midPoint.transform.forward * 100));

    //laserLine.SetPosition(1, leftEye.position + new Vector3(0, 0, 4));
    //laserLine2.SetPosition(1, rightEye.position+ new Vector3(0, 0, 4));
    //laserLine.SetPosition(1, rightEye.position);

    Vector3 newPos;
    
    leftEyeEndPoint.position = leftIris.transform.GetChild(0).position;
    newPos = leftEyeEndPoint.position;
    newPos.z = -100f;
    leftEyeEndPoint.position = newPos;
    laserLine.SetPosition(1, leftEyeEndPoint.position /*+ -(leftEyeEndPoint.transform.forward * 100)*/);

    if (Physics.Raycast(leftEye.position, leftEye.TransformDirection(Vector3.forward), out hitL, Mathf.Infinity))
    {
      Debug.DrawRay(leftEye.position, leftEye.TransformDirection(Vector3.back) * hitL.distance, Color.yellow);
      
      averageHitPoint = hitL.point;
    }

    if (Physics.Raycast(rightEye.position, rightEye.TransformDirection(Vector3.forward), out hitR, Mathf.Infinity))
    {
      Debug.Log("454545");
      Debug.DrawRay(rightEye.position, rightEye.TransformDirection(Vector3.forward) * hitR.distance, Color.yellow);
      laserLine.SetPosition(1, rightEye.position);
      averageHitPoint = hitR.point;
    }

    averageHitPoint = (hitL.point + hitR.point) / 2f;
    
    //laserLine.SetPosition(1, rayOrigin + (fpsCam.transform.forward * weaponRange));
  }

}
